'use strict'

const express = require('express');
const { route } = require('express/lib/application');
const deviceController = require('../controllers/deviceController');
const router = express.Router();

const { getDevices } = deviceController;

router.get('/devices', getDevices);

module.exports = {
    routes: router
}