'use strict'

const express = require('express');
const { route } = require('express/lib/application');
const rastaurantController = require('../controllers/rastaurantController');
const router = express.Router();

const { getRastaurant } = rastaurantController;

router.get('/postcode/:id', getRastaurant);

module.exports = {
    routes: router
}