'use strict'
const dotenv = require('dotenv');
const assert = require('assert');

dotenv.config();

const { PORT, HOST, HOST_URL, SQL_USER, SQL_PASS, SQL_DB, SQL_SERVER } = process.env;

const sqlEncrypt = process.env.ENCRYPT === "true"

assert(PORT, 'PORT is required');
assert(HOST, 'HOST is required');

module.exports = {
    port: PORT,
    host: HOST,
    url: HOST_URL,
    sql: {
        user: SQL_USER,
        password: SQL_PASS,
        server: SQL_SERVER,
        database: SQL_DB,
        port: 1433,
        options: {
            encrypt: false,
            trustServerCertificate: false
        }
    }
};