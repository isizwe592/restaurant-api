'use strict'

const rastaurantData = require('../data/rastaurants');

const getRastaurant = async (req, res, next) => {
    try {
        const postcode = req.params.id;
        const record = await rastaurantData.getRastaurantById(postcode);

        res.send(record);
    } catch (error) {
        res.status(400).send(error.message);
    }
}

module.exports = {
    getRastaurant
}