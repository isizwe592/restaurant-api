'use strict'

const deviceData = require('../data/tblDevice');

const getDevices = async (req, res, next) => {
    try {
        const devices = await deviceData.getDevices();
        res.send(devices);
    } catch (error) {
        return error.message;
    }
}

module.exports = {
    getDevices
}