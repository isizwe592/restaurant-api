SELECT 
	dift.Id,
	di.Name [name], 
	di.Review [rating], 
	ft.Name [foodtype],
	di.imageName
FROM DeliveryInfoFoodTypes dift
	INNER JOIN DeliveryInfo di ON dift.DeliveryId = di.Id
	INNER JOIN FoodTypes ft ON dift.FoodTypeId = ft.Id
WHERE 
	di.PostCode =@postcode;
