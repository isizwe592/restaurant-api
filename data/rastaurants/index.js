'use strict'

const utils = require('../utils');
const config = require('../../config');
const sql = require('mssql');
const res = require('express/lib/response');

const getRastaurantById = async (postcode) => {
    try {
        let pool = await sql.connect(config.sql);
        const sqlQueries = await utils.loadSqlQueries('rastaurants');
        const record = await pool.request()
                         .input('postcode', sql.VarChar, postcode)
                         .query(sqlQueries.rastaurantbycode);

        return record.recordset;
    } catch (error) {
        return error.message;
    }
}

module.exports = {
    getRastaurantById
}