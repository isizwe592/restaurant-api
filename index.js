'use strict'
const express = require('express');
const config = require('./config');
const cors = require('cors');
const bodyParser = require('body-parser');
const eventRoutes = require('./routes/eventRoutes');
const deviceRoutes = require('./routes/deviceRoutes');
const rastaurantRoutes = require('./routes/rastaurantRoutes');

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use('/api', eventRoutes.routes);
app.use('/rastaurant', rastaurantRoutes.routes);

app.listen(config.port, () => console.log('Server is running on ' + config.url));